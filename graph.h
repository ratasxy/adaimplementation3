#ifndef GRAPH_H
#define GRAPH_H

#include <vector>

using namespace std;

class Graph
{
    friend class Components;
    friend class LongestGame;
    friend class Bridges;
public:
    Graph();
    Graph(size_t n);
    inline size_t n() const {
        return adjacency_.size();
    }
    inline size_t m() const {
        return m_;
    }
    void addEdge(int u, int v);

private:
    vector<vector<int>> adjacency_;
    size_t m_;
};

typedef pair<int, int> Edge;

#endif // GRAPH_H
