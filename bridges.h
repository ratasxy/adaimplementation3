#ifndef BRIDGES_H
#define BRIDGES_H

#include "graph.h"

class Bridges
{
public:
    Bridges(const Graph &graph);
    int operator()(int src);

private:
    void dfs(int u, int parent=-1);
    const Graph *graph_;
    int dfs_counter_;
    vector<int> dfs_num_, dfs_low_;
    int bridge_count_;
};

#endif // BRIDGES_H
