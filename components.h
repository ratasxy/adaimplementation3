#ifndef COMPONENTS_H
#define COMPONENTS_H

#include <stack>

#include "graph.h"

class Components
{
public:
    typedef pair<int, int> ComponentDescription;
    Components(Graph * graph);
    vector<ComponentDescription> operator()();
private:
    Graph * graph;
    vector<bool> visitedList_;
    ComponentDescription dfs(int);
};

#endif // COMPONENTS_H
