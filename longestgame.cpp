#include "longestgame.h"

LongestGame::LongestGame(const Graph &graph) : graph_(&graph)
{
}

int LongestGame::operator()(int s)
{
    distance_.assign(graph_->n(), graph_->n()+1);
    parent_.assign(graph_->n(), -1);
    queue<int> q;
    q.push(s);
    distance_[s] = 0;
    int u = s;
    while (not q.empty()) {
        u = q.front();
        q.pop();
        for (int v : graph_->adjacency_[u]) {
            if (distance_[v] > distance_[u] + 1) {
                distance_[v] = distance_[u] + 1;
                parent_[v] = u;
                q.push(v);
            }
        }
    }
    return u;
}

int LongestGame::distance(int u)
{
    return distance_[u];
}

vector<int> LongestGame::path(int s, int t)
{
    vector<int> path;
    while (t != s) {
        path.push_back(t);
        t = parent_[t];
    }
    path.push_back(t);
    reverse(path.begin(), path.end());
    return path;
}
