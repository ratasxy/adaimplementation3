#ifndef LONGESTGAME_H
#define LONGESTGAME_H

#include <queue>
#include <algorithm>

#include "graph.h"

class LongestGame
{
public:
    LongestGame(const Graph &graph);
    int operator()(int s);
    int distance(int u);
    vector<int> path(int s, int t);
private:
    const Graph *graph_;
    vector<int> distance_;
    vector<int> parent_;
};

#endif // LONGESTGAME_H
