#include "components.h"

Components::Components(Graph * graph)
{
    this->graph = graph;
}

Components::ComponentDescription Components::dfs(int start)
{
    ComponentDescription description, tmp;
    visitedList_[start] = true;
    ++description.first;
    description.second += graph->adjacency_[start].size();
    for(int i : this->graph->adjacency_[start]) {
        if(not visitedList_[i]) {
            tmp = this->dfs(i);
            description.first += tmp.first;
            description.second += tmp.second;
        }
    }
    return description;
}

vector<Components::ComponentDescription> Components::operator()()
{
    visitedList_.assign(this->graph->n(), false);

    vector<ComponentDescription> componentDescriptions;
    for(size_t i = 0; i < this->graph->n(); ++i) {
        if(not visitedList_[i])
            componentDescriptions.push_back(this->dfs(i));
    }
    return componentDescriptions;
}
