TEMPLATE = app
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    game.cpp \
    graph.cpp \
    bridges.cpp \
    components.cpp \
    longestgame.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    game.h \
    graph.h \
    bridges.h \
    components.h \
    longestgame.h

