#include <iostream>
#include <chrono>

#include "game.h"
#include "longestgame.h"
#include "components.h"
#include "bridges.h"

using namespace std;

Game game;
Graph graph;

ostream& operator<<(ostream &os, vector<int> data)
{
    os << '[';
    if (data.size()) {
        os << data[0];
        for (size_t i = 1; i < data.size(); ++i)
            os << ", " << data[i];
    }
    os << ']';
    return os;
}

chrono::time_point<chrono::system_clock> startTime, endTime;

void buildGraph()
{
    cout << "== BUILDING GRAPH ===" << endl;
    startTime = chrono::system_clock::now();
    graph = game.generateGraph();
    endTime = chrono::system_clock::now();
    int ms = chrono::duration_cast<chrono::milliseconds>(endTime-startTime).count();
    cout << "Time: " << ms << endl;
    cout << "=====================" << endl;
}

void firstQuestion()
{
    cout << "== FIRST QUESTION ===" << endl;
    Components componentsExtractor(&graph);
    startTime = chrono::system_clock::now();
    vector<Components::ComponentDescription> components = componentsExtractor();
    endTime = chrono::system_clock::now();
    for (size_t i = 0; i < components.size(); ++i)
        cout << "Component " << i + 1 << " Vertices: " << components[i].first <<
                " Edges: "<< components[i].second / 2 << endl;
    int ms = chrono::duration_cast<chrono::milliseconds>(endTime-startTime).count();
    cout << "Time: " << ms << endl;
    cout << "=====================" << endl;
}

void secondQuestion()
{
    cout << "== SECOND QUESTION ==" << endl;
    LongestGame furthest(graph);
    startTime = chrono::system_clock::now();
    int src = 0, dst = furthest(src);
    endTime = chrono::system_clock::now();
    int ms = chrono::duration_cast<chrono::milliseconds>(endTime-startTime).count();
    cout << "Distance: " << furthest.distance(dst) << endl;
    for (int pid : furthest.path(src, dst)) {
        cout << game.numToPerm(pid) << endl;
    }
    cout << "Time: " << ms << endl;
    cout << "=====================" << endl;
}

void thirdQuestion()
{
    cout << "== THIRD QUESTION ===" << endl;
    Bridges bridges(graph);
    startTime = chrono::system_clock::now();
    cout << "Bridges: " << bridges(0) << endl;
    endTime = chrono::system_clock::now();
    int ms = chrono::duration_cast<chrono::milliseconds>(endTime-startTime).count();
    cout << "Time: " << ms << endl;
    cout << "=====================" << endl;
}

int main()
{
    buildGraph();
    firstQuestion();
    secondQuestion();
    thirdQuestion();
    return 0;
}
