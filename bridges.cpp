#include "bridges.h"

Bridges::Bridges(const Graph &graph) : graph_(&graph)
{
}

int Bridges::operator()(int src)
{
    dfs_low_.resize(graph_->n());
    dfs_num_.assign(graph_->n(), -1);
    dfs_counter_ = 0;
    bridge_count_ = 0;
    dfs(src);
    return bridge_count_;
}

void Bridges::dfs(int u, int parent)
{
    dfs_low_[u] = dfs_num_[u] = dfs_counter_++;
    for (int v : graph_->adjacency_[u]) {
        if (dfs_num_[v] == -1) {
            dfs(v, u);
            if (dfs_low_[v] > dfs_num_[u]) {
                bridge_count_++;
            }
            dfs_low_[u] = min(dfs_low_[u], dfs_low_[v]);
        } else if (v != parent) {
            dfs_low_[u] = min(dfs_low_[u], dfs_num_[v]);
        }
    }
}
