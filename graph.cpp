#include "graph.h"

Graph::Graph()
{
    m_ = 0;
}

Graph::Graph(size_t n) : adjacency_(n)
{
    m_ = 0;
}

void Graph::addEdge(int u, int v)
{
    adjacency_[u].push_back(v);
    ++m_;
}
