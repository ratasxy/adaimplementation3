#ifndef GAME_H
#define GAME_H

#include <vector>
#include <list>
#include <algorithm>
#include "graph.h"

using namespace std;

class Game
{
public:
    static const int side, size;
    Game();
    int permToNum(const vector<int> &perm);
    vector<int> numToPerm(int num);
    vector<vector<int>> adjacentPerms(const vector<int> &perm);
    inline int graphSize() {
        return fact_[size];
    }
    Graph generateGraph();

private:
    vector<int> order_;
    static const int dx_[], dy_[];
    int fact_[10];
    inline bool validCoord(int x, int y) {
        return x >= 0 and x < side and y >= 0 and y < side;
    }
};

#endif // GAME_H
