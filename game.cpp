#include "game.h"

const int Game::dx_[] = {-1, 0, 0, 1};
const int Game::dy_[] = {0, -1, 1, 0};
const int Game::side = 3;
const int Game::size = side * side;

Game::Game()
{
    order_.resize(9);
    for (int i = 0; i < 9; ++i)
        order_[i] = i;
    fact_[0] = 1;
    for (int i = 1; i <= size; ++i)
        fact_[i] = fact_[i-1] * i;
}

int Game::permToNum(const vector<int> &perm)
{
    vector<int> order(order_);
    int num = 0;
    for (size_t i = 0; i < perm.size(); ++i) {
        auto it = find(order.begin(), order.end(), perm[i]);
        num += (it - order.begin())* fact_[8-i];
        order.erase(it);
    }
    return num;
}

vector<int> Game::numToPerm(int num)
{
    vector<int> order(order_);
    vector<int> perm(size);
    for (int i = 0; i < size; ++i) {
        int idx = num / fact_[8-i];
        num %= fact_[8-i];
        perm[i] = order[idx];
        order.erase(order.begin()+idx);
    }
    return perm;
}

vector<vector<int>> Game::adjacentPerms(const vector<int> &perm)
{
    int idx = find(perm.begin(), perm.end(), 8) - perm.begin();
    int x = idx % 3, y = idx / 3;
    vector<vector<int>> adj;
    for (int i = 0; i < 4; ++i) {
        int xt = x + dx_[i], yt = y + dy_[i];
        if (validCoord(xt, yt)) {
            int idx_t = yt * 3 + xt;
            vector<int> perm_t(perm);
            swap(perm_t[idx], perm_t[idx_t]);
            adj.push_back(perm_t);
        }
    }
    return adj;
}

Graph Game::generateGraph()
{
    vector<int> perm(order_);
    int u = 0;
    Graph graph(graphSize());
    do {
        for (vector<int> &perm_t : adjacentPerms(perm))
            graph.addEdge(u, permToNum(perm_t));
        ++u;
    } while(next_permutation(perm.begin(), perm.end()));
    return graph;
}
